

export default class storeage{

    public static storeageSave<T>(key: string, value: T): void{
        sessionStorage.setItem(key, JSON.stringify(value));
    }
    public static storeageRead<T>(key: string): T | null {
       const storeValue = sessionStorage.getItem(key);
       try{
           
           if(storeValue){
               return JSON.parse(storeValue) as T;
           }else{
               return null;
           }
       }
       catch(err){
            sessionStorage.removeItem(key);
            return null;
       }
    }
}