import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'ng-pokemon-trainer';
  clickamout : number = 0;
  clickIncreaser(){
    console.log(this.clickamout)
    this.clickamout++;
  }
}
