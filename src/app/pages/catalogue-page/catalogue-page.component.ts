import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';
import LoginService from 'src/app/services/login.service';
import storage from '../../utils/storage.util';
import { Router } from '@angular/router';
import { trainer } from '../../models/trainer.mode';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css']
})
export class CataloguePageComponent implements OnInit {
  public pokemonNames: string[] = [];
  public pokemonImg: string[] = [];
  public pokemonArr: any[][]= [];
  constructor(private readonly router: Router, private readonly pokemon: PokemonService, private readonly loginService: LoginService) { }

  ngOnInit(): void {
    if(storage.storeageRead("Trainer") === null) {
      this.router.navigateByUrl('/catalogue')
    }
    this.pokemon.getPokemons().subscribe(
      (next)=>{
        

        const keys = Object.keys(next.results);

        keys.forEach((key, index) => {
          this.pokemonNames.push(next.results[key].name)

          let tempOne = next.results[key].url.replace("https://pokeapi.co/api/v2/pokemon/","")
          let tempTwo = tempOne.replace("/","");
          this.pokemonImg.push(`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/${tempTwo}.gif`)
       
          this.pokemonArr.push([next.results[key].name,`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/${tempTwo}.gif`])
          
          
          
        });
        
      },
    );
  }
  onCatch(e: string[]){
    console.log("click",e);
    let trainer:any  = storage.storeageRead("Trainer")
    if(trainer){
      console.log("storage.storeageRead(Trainer)  ", trainer.id)
    }
    
    this.loginService.addPokemon(trainer.id,[e[0],e[1]]).subscribe(
      next => {console.log("addPokemon ",next)
          
      storage.storeageSave("Trainer", next)
      }
    );

  }

}
