import { Component, OnInit } from '@angular/core';
import storage from '../../utils/storage.util';
import LoginService from 'src/app/services/login.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {
  public trainername: any | null;
  public pokemon: any | null;
  public trainernameId: any | null;
  constructor(private readonly loginService: LoginService) {
   
    let trainer : any = storage.storeageRead("Trainer")
    this.trainername = trainer.trainername
    this.pokemon = trainer.pokemon
    this.trainernameId = trainer.id
 
    
   }

  ngOnInit(): void {
    
    this.loginService.getTrainerByID(this.trainernameId).subscribe(
      next => {
        console.log("getTrainer",next)
        this.pokemon = next.pokemon;
        console.log(this.pokemon[0][1])
      }
    )
  }
  deletePokemon(e: string[]){
    
    this.loginService.deletePokemon(parseInt(this.trainernameId),e[0],this.pokemon).subscribe(
      next => {
        
        this.pokemon = next.pokemon;
      }
    )
  }
}
