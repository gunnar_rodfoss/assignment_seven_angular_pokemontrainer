
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import LoginService from 'src/app/services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private readonly router: Router,private readonly loginService: LoginService) { }

  ngOnInit(): void {
  }
  onSubmit(loginform: NgForm): void {
    const { trainername } = loginform.value;
    console.log(trainername);
    
    this.loginService.loginTrainer(trainername).subscribe(
      next => {
        console.log(next)
        this.router.navigateByUrl('/catalogue')
      },
      
    );


  }

}
