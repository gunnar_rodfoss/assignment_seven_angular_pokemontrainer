import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }
  onLogout(){
    console.log("Login out")
    sessionStorage.clear()
    this.router.navigateByUrl('/')
  }
  cataloguePage(){
    this.router.navigateByUrl('/catalogue')
  }
  trainerPage(){
    this.router.navigateByUrl('/trainer')
  }
}
