import { Component, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, Observable, switchMap,pipe,of,tap } from 'rxjs';
import { environment } from '../../environments/environment'
import { trainer } from '../models/trainer.mode';

import storeage from '../utils/storage.util'

@Injectable({
  providedIn: 'root'
})
export default class LoginService {
  public trainerUrl = environment.apiTrainers;
  public trainerKey = environment.apiKey;
  public trainerobj:any;
  constructor(private http: HttpClient) {
    
  }

  public getTrainers(): Observable<any> {
    return this.http.get<any>(this.trainerUrl)
  }


  public deletePokemon(trainerId:number, pokemonName: string, pokemon: string[]): Observable<any> {
    
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": this.trainerKey
    });

    
    let newPokemonArr = pokemon.filter(e => e[0] !== pokemonName)
    return this.http.patch<trainer>(`${this.trainerUrl}/${trainerId}`, { pokemon: [...newPokemonArr] }, {
      headers
    })
  }
  public loginTrainer(trainername: string): Observable<trainer> {
    return this.checkTrainername(trainername)
      .pipe(
        switchMap((trainer: trainer | undefined) => {
          console.log("check for trainer ",trainer)
          if (trainer === undefined) {
            return this.createTrainer(trainername);
          }
          else{
            
            return of(trainer);
          }
        }),
        tap((trainer: trainer) => {
          storeage.storeageSave("Trainer", trainer);
          this.trainerobj = trainer;  
        })
      )
  }

  public checkTrainername(trainername: string): Observable<trainer | undefined> {
    return this.http.get<trainer[]>(`${this.trainerUrl}?trainername=${trainername}`)
      .pipe(
        map((response: trainer[]) => {
          console.log("check for trainer ",response.pop())
          return response.pop();
        })
      )
  }

  public createTrainer(trainername: string): Observable<trainer> {

    const trainer = {
      trainername,
      pokemon: [],
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": this.trainerKey
    });


    return this.http.post<trainer>(this.trainerUrl, trainer, {
      headers
    })
  }
  public getTrainerByID(trainerId: number): Observable<trainer>{
    return this.http.get<trainer>(`${this.trainerUrl}/${trainerId}`)
  }
  public addPokemon(trainerId:number, pokemonName: string[]): Observable<trainer> {

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": this.trainerKey
    });

    this.trainerobj.pokemon.push(pokemonName)
    return this.http.patch<trainer>(`${this.trainerUrl}/${trainerId}`, { pokemon: [...this.trainerobj.pokemon] }, {
      headers
    })
  }
}
