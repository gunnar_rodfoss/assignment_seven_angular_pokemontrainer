import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { pokemon } from '../models/pokemon.model';
import { map, Observable, switchMap,pipe,of,tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private pokemonAPI : any = environment.apiPokemon
  private pokemonGifAPI: any = environment.apiPokemonGIF
  constructor(private readonly http:HttpClient) { }

  getPokemons():Observable<any>{
    return this.http.get(`${this.pokemonAPI}?limit=100&offset=0`)
  }
  getPokemonsGIF(pokemonID:number):any{
    return this.http.get(`${this.pokemonGifAPI}${pokemonID}.gif`)
  }
}
