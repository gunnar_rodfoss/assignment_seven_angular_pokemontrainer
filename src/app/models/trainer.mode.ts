import { pokemon } from "./pokemon.model";
export interface trainer {
    
        id: number;
        username: string;
        pokemon: pokemon;
    }
