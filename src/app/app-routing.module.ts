import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginPageComponent} from "./pages/login-page/login-page.component"
import { TrainerPageComponent } from "./pages/trainer-page/trainer-page.component"
import { CataloguePageComponent } from "./pages/catalogue-page/catalogue-page.component"
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [

  {
    path:'',
    component: LoginPageComponent,
  },
  {
    path:'trainer',
    component: TrainerPageComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'catalogue',
    component: CataloguePageComponent,
    canActivate:[AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
